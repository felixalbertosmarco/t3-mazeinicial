.PHONY: all
all: dist/index.html
	tsc
dist/index.html: dist src/index.html
	cp src/index.html dist/index.html
dist:
	mkdir dist
