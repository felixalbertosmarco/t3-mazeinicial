import {Directions} from './Directions.js';
import {Door} from './Door.js';
import {Maze} from './Maze.js';
import {Room} from './Room.js';
import {Wall} from './Wall.js';
export class MazeGame{
  constructor(){
    console.log("Tu nuevo mazegame !!!!");
  }
  public createMaze():Maze{
    let maze:Maze = new Maze();
    let r1:Room = new Room(1);
    let r2:Room = new Room(2);
    maze.addRoom(r1);
    maze.addRoom(r2);
    let door:Door = new Door(r1,r2);

    r1.setSide(new Wall(),Directions.North);
    r1.setSide(new Wall(),Directions.South);
    r1.setSide(door,Directions.East);
    r1.setSide(new Wall(),Directions.West);

    r2.setSide(new Wall(),Directions.North);
    r2.setSide(new Wall(),Directions.South);
    r2.setSide(new Wall(),Directions.East);
    r2.setSide(door,Directions.West);

    console.log(door.otherSideFrom(r2));

    return maze;
  }
}

