import {Room} from './Room.js';
import {MapSite} from './MapSite.js';
export class Door extends MapSite{
  private isOpen:boolean;
  private room1:Room;
  private room2:Room;
  constructor(r1:Room, r2:Room){
    super();
    this.room1=r1;
    this.room2=r2;
    this.isOpen=true;
  }
  public enter():void{
    console.log("Has entrado en la puerta");
  }
  public otherSideFrom(r:Room):MapSite{
    if(r==this.room1){
      return this.room2; 
    }
    return this.room1;
  }
}

